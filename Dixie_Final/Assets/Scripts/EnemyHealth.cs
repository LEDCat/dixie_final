﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyHealth : MonoBehaviour
{
    public int health = 1; //amount of health
    public Transform player;
    public Transform projectile;
    public GameEnding gameEnding;

    public void TakeDamage(int damageAmount)
    {
        health -= damageAmount;

        if (health <= 0)
        {
            Destroy(gameObject); //kills enemy once health reaches 0
        }
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("enemy hit");
        if (other.transform == player)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        else
        {
            if (other.transform == projectile)
            {
                TakeDamage(1);
            }
        }
        /*
        if (other.transform == projectile)
        {
            TakeDamage(1);
        }
        */
    }
}