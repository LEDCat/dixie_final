﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileFile : MonoBehaviour
{
    public void OnTriggerEnter2D(Collider2D other)
    {
        //when the bullet collides with something
        if (!other.isTrigger) //ignores trigger colliders
        {
            if (other.gameObject.CompareTag("Enemy"))
            {
                //if bullet hits enemy
               EnemyHealth eHealth = other.gameObject.GetComponent<EnemyHealth>(); //returns reference of enemy health

               if (eHealth != null)
                    eHealth.TakeDamage(1); //cause one damage to enemy health
                    Debug.Log("bubble hit");
                    Destroy(gameObject);
            }
            //Destroy(gameObject);//destroys projectile
        }
    }
}
