﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEnding : MonoBehaviour
{
    public float fadeDuration = 1f;
    public GameObject player;
    public CanvasGroup GameOverBGCanvasGroup;
    public CanvasGroup GameWinBGCanvasGroup;

    bool m_IsPlayerAlive;
    bool m_IsPlayerDead;
    bool m_IsPlayerExit;
    float m_Timer;
    public static float timer;


    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            m_IsPlayerExit = true;
        }
    }

    private void Start()
    {
        // Starts the timer automatically
        timer = Timer.timeLeft;
    }
    public void DeadPlayer()
    {
        m_IsPlayerDead = true;
    }
    public void AlivePlayer()
    {
        m_IsPlayerAlive = true;
    }

    void Update()
    {
        if (m_IsPlayerDead)
        {
            EndLevel(GameOverBGCanvasGroup, true);
        }
        
        if (timer <= 0)
        {
            EndLevel(GameOverBGCanvasGroup, true);
        }
        else if (m_IsPlayerExit)
        {
            EndLevel(GameWinBGCanvasGroup, true);
            Application.Quit();
        }
        /*
        if (m_IsPlayerAlive) //will check if player is alive for the boss fight
        {
            EndLevel(GameOverBGCanvasGroup, false);

            if (gameObject.CompareTag("Boss"))
            {
                GameObject Boss = GameObject.Find("Boss");
                BossHealth bHealth = Boss.GetComponent<BossHealth>();
                if (bHealth.health == 0) //will bring up win screen if boss is killed
                    EndLevel(GameWinBGCanvasGroup, true);
            }
        }
        */ //this chunk was originally meant to make sure the player was still alive before triggering the boss fight
    }
    void EndLevel(CanvasGroup imageCanvasGroup, bool doRestart)
    {
        m_Timer += Time.deltaTime;

        imageCanvasGroup.alpha = m_Timer / fadeDuration;

        if(doRestart) //gives player choice to try again or quit game
        {
            if (Input.GetKeyDown(KeyCode.Y)) {
                SceneManager.LoadScene(0);
                timer = 60;
            }
            if (Input.GetKeyDown(KeyCode.N))
            {
                Application.Quit();
                //Debug.Log("quit");
            }
        }
        else
        {
            Application.Quit();
        }
    }
}
