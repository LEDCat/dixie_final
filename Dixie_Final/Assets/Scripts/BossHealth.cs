﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossHealth : MonoBehaviour
{
    public int health = 1; //amount of health
    public Transform player;
    public GameEnding gameEnding;

    public void TakeDamage(int damageAmount)
    {
        health -= damageAmount;

        if (health <= 0)
        {
            Destroy(gameObject); //kills enemy once health reaches 0
            gameEnding.AlivePlayer();
        }
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform == player)
        {
            gameEnding.DeadPlayer();
        }
    }
}
