﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public static float timeLeft = 60;
    public bool m_timerRunning = false;
    public GameEnding gameEnding;
    public TextMeshProUGUI timeText;

    private void Start()
    {
        // Starts the timer automatically
        m_timerRunning = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (m_timerRunning) //bool used to make sure timer stops running fully when it reaches 0
        {
            if (timeLeft > 0)
            {
                timeLeft -= Time.deltaTime;
                ShowTime(timeLeft);
            }
            else
            {
                timeLeft = 60;
                m_timerRunning = false;
                gameEnding.DeadPlayer();
            }
        }
    }

    //sets string to display timer in game
    void ShowTime(float timeToDisplay)
    {
        timeToDisplay += 1;

        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        timeText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}
