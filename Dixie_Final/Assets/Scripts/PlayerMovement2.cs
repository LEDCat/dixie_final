﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement2 : MonoBehaviour
{
    //Use a vector
    //apply vector to a rigidboy using numbers from input

    Rigidbody2D rB2D;

    public float runSpeed;
    public float jumpForce;

    public SpriteRenderer spriteRenderer;
    public Animator animator;

    public GameObject projectilePrefab; //projectile prefab to be cloned
    public Transform shotSpawn; //spwans where the projectiles will form
    public float shotSpeed = 20f;

    void Start()
    {
        rB2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (transform.position.y < -5f) //if player has fallen far enough
            //reloads game scene
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        if (Input.GetButtonDown("Jump"))
        {
            int levelMask = LayerMask.GetMask("Level");

            if (Physics2D.BoxCast(transform.position, new Vector2(1f, .1f), 0f, Vector2.down, .01f, levelMask))
            {
                Jump();
            }
        }

        else if (Input.GetMouseButtonDown(0))
        {
            //creates projectile
            GameObject projectile = Instantiate(projectilePrefab, shotSpawn.transform.position, projectilePrefab.transform.rotation);
            //gets projectile's rigidbody to give it velocity, flying forward
            Rigidbody2D projectileRB = projectile.GetComponent<Rigidbody2D>();
            projectileRB.velocity = transform.right * shotSpeed;
        }
    }
    private void FixedUpdate()
    {
        //Apply horizontal run movement
        float horizontalInput = Input.GetAxis("Horizontal");

        rB2D.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime, rB2D.velocity.y);

        if (rB2D.velocity.x < 0)
            spriteRenderer.flipX = false; //going left, flips sprite to face direction
        else
        if (rB2D.velocity.x > 0)
            spriteRenderer.flipX = true; //going right, flips sprite to face direction

        //do run animation when moving left and right
        if (Mathf.Abs(horizontalInput) > 0f)
            animator.SetBool("IsRunning", true);
        else
            animator.SetBool("IsRunning", false);
    }
    void Jump()
    {
        rB2D.velocity = new Vector2(rB2D.velocity.x, jumpForce);
    }
}